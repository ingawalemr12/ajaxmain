<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_Model extends CI_Model {

	public function create($formArray)
	{
		$this->db->insert('car_models', $formArray);
		$id = $this->db->insert_id(); // insert record with id
		return $id;
	}

	public function all()
	{
		$result = $this->db->order_by('id', 'ASC')->get('car_models')->result_array();
		return $result;
	}

	public function getRow($id) 
	{
		$this->db->where('id', $id);  // fetch record with id
		$row = $this->db->get('car_models')->row_array();
		return $row;
	}
}