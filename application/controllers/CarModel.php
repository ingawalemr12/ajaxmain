<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CarModel extends CI_Controller {

	public function index()
	{
		$this->load->model('Car_Model');
		$rows = $this->Car_Model->all();
		$data['rows'] = $rows;
		$this->load->view('car_model/list', $data);
	}

	public function showCreateform()
	{
		$html = $this->load->view('car_model/create.php','',true);
		$response['html'] = $html;
		echo json_encode($response);
	}

	public function saveModel()
	{
		$this->load->model('Car_Model');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('color', 'Color', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');

		if($this->form_validation->run() == true) {
			# save entries in db table
			$formArray=array();
			$formArray['name'] = $this->input->post('name');
			$formArray['price'] = $this->input->post('price');
			$formArray['transmission'] = $this->input->post('transmission');
			$formArray['color'] = $this->input->post('color');
			$formArray['created_at'] = date('Y-m-d H:i:s');
			$id = $this->Car_Model->create($formArray);  // insert record with id

			$row = $this->Car_Model->getRow($id); 		// fetch record with id
			$vdata['row'] = $row;
			$rowhtml = $this->load->view('car_model/car_row', $vdata, true);
			$response['row'] = $rowhtml; 

			$response['status'] = 1;
			$response['message'] = "<div class='alert alert-success'>Record has been added successfully</div>";   //show alert message after data is inserted in db table
		}
		else
		{
			# show error message
			$response['status'] = 0;
			$response['name']= strip_tags(form_error('name'));
			$response['color']= strip_tags(form_error('color'));
			$response['price']= strip_tags(form_error('price'));
		}
		 echo json_encode($response);
	}
	
}
