<!DOCTYPE html>
<html>
<head>
<title>AJAX Basic</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
</head>
<body>
	<div class="header">
		<div class="container">
			<h3 class="heading">AJAX CRUD APPLICATION</h3>
		</div>
	</div>
	<div class="container">
		<div class="row pt-3">
			<div class="col-md-6">
				<h4>Car Model</h4>
			</div>
			<div class="col-md-6 text-right">
				<a href="javascript:void(0);" onclick="showModal()" class="btn btn-primary">Create</a>
			</div>

			<div class="col-md-12 pt-3">
				<table class="table table-striped" id="carModelList">
					<tbody>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Color</th>
							<th>Transmission</th>
							<th>Price</th>
							<th>Created_at</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>

						<?php 
						if (!empty($rows)) {
							foreach ($rows as $row) { 
							// shows fetch data is appended  i.e. AJAX
								$data['row'] = $row;
								$this->load->view('car_model/car_row', $data);
							}
						} else {?>
							<tr>
								<td>Record Not found</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal to insert data in table -->
	<div class="modal fade" id="createCar" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">AJAX CRUD Modal</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div id="response">
	      	
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal to show Alert Message -->
	<div class="modal fade" id="ajaxResponseModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Alert Message</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      
	      	<div class="modal-body">

	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
		        </button>
			</div>
	      
	    </div>
	  </div>
	</div>

	<script type="text/javascript">
		function showModal(){
			$("#createCar").modal("show");
			$.ajax({
				url: '<?php echo base_url().'CarModel/showCreateform' ?>',
				type: 'POST',
				data:{},
				dataType: 'json',
				success : function(response){
					//console.log(response); 
				  $("#response").html(response["html"]);
				}
			});
		}

		$("body").on("submit", "#createCarModel", function(e){
			e.preventDefault();
			//alert();

			$.ajax({
				url: '<?php echo base_url('CarModel/saveModel') ?>',
				type: 'POST',
				data:$(this).serializeArray(),
				dataType: 'json',
				success: function(response){

					if (response['status'] == 0) {
						if (response["name"] !="") {
						  $(".nameError").html(response["name"]).addClass('invalid-feedback d-block');
						  $("#name").addClass('is-invalid');
						}
						else
						{
						  $(".nameError").html("").removeClass('invalid-feedback d-block');
						  $("#name").removeClass('is-invalid');
						}

						if (response["color"] !="") {
						  $(".colorError").html(response["color"]).addClass('invalid-feedback d-block');
						  $("#color").addClass('is-invalid');
						}
						else
						{
						  $(".colorError").html("").removeClass('invalid-feedback d-block');
						  $("#color").removeClass('is-invalid');
						}

						if (response["price"] !="") {
						  $(".priceError").html(response["price"]).addClass('invalid-feedback d-block');
						  $("#price").addClass('is-invalid');
						}
						else
						{
						  $(".priceError").html("").removeClass('invalid-feedback d-block');
						  $("#price").removeClass('is-invalid');
						}
					}
					else
					{
						//show alert message after data is inserted in db table
						$("#createCar").modal("hide");
						$("#ajaxResponseModal .modal-body").html(response["message"]);
						$("#ajaxResponseModal").modal("show");

						//show form validation, text fields are selected
						$(".nameError").html("").removeClass('invalid-feedback d-block');
						$("#name").removeClass('is-invalid');

						$(".colorError").html("").removeClass('invalid-feedback d-block');
						$("#color").removeClass('is-invalid');

						$(".priceError").html("").removeClass('invalid-feedback d-block');
						$("#price").removeClass('is-invalid');
					}

					// shows inserted data is appended , without refresh the page i.e. AJAX
					$("#carModelList").append(response["row"]);
					
				}
			});
		});

		
	</script>
</body>
</html>